import {Component, Inject, OnInit} from '@angular/core';
import {ConfigService} from '../_demo-core/config.service';
import {FormGroup} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {MatSnackBar} from '@angular/material/snack-bar';
import {DemoConfig} from '../_demo-core/_demo-core';
import {ControlSchema} from '../_demo-core/form-generator/form-generator.component';

@Component({
  selector: 'app-auth-panel',
  templateUrl: './auth-panel.component.html',
  styleUrls: ['./auth-panel.component.scss']
})
export class AuthPanelComponent implements OnInit {
  public config: DemoConfig;
  public showLoginView: boolean = !this.data?.signup;
  public form: FormGroup;
  public loginFormSchema: ControlSchema[] = [
	{name: 'email', type: 'email', label: 'Email', validators: ['email', 'required'], icon: 'mail'},
	{name: 'password', type: 'password', label: 'Password', icon: 'lock', validators: ['required']},
  ];
  public signupFormSchema: ControlSchema[] = [
	{name: 'banner_image', type: 'hidden'},
	{name: 'profile_image', type: 'hidden'},
	{name: 'email', type: 'email', label: 'Email', validators: ['email', 'required'], icon: 'mail'},
	{name: 'username', type: 'text', label: 'Username', validators: ['required'], icon: 'person'},
	{name: 'password', type: 'password', label: 'Password', icon: 'lock', validators: ['required']},
  ];

  constructor(
	  private configService: ConfigService,
	  private dialogRef: MatDialogRef<AuthPanelComponent>,
	  private snackBar: MatSnackBar,
	  @Inject(MAT_DIALOG_DATA) public data
  ) {
  }

  ngOnInit(): void {
	this.config = this.configService.config;
  }

  public submit(): void {
	this.form.markAllAsTouched();
	if (this.form.valid) {
	  const endpoint = this.showLoginView ? '/auth/login' : '/auth/signup';
	  this.configService.xanoAPI(endpoint, 'post', this.form.getRawValue()).subscribe(authToken => {
		if (authToken) {
		  this.configService.authToken.next(authToken.authToken);
		  this.configService.xanoAPI('/auth/me', 'get', null, {}, true)
			  .subscribe(
				  res => this.configService.user.next(res),
				  error => this.configService.showErrorSnack(error)
			  );
		  this.dialogRef.close();
		} else {
		  this.snackBar.open('No Auth Token', 'Error', {panelClass: 'error-snack'});
		}
	  }, error => this.configService.showErrorSnack(error));
	}
  }

  public upload(formData, controlName): void {
	this.configService.xanoAPI('/upload/image', 'post', formData).subscribe(
		res => this.form.get(controlName).patchValue(res),
		error => this.configService.showErrorSnack(error)
	);
  }

  public deleteImage(controlName): void {
	this.form.get(controlName).patchValue(null);
  }

}
