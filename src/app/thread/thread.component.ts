import {Component, OnInit} from '@angular/core';
import {ConfigService} from '../_demo-core/config.service';
import {ActivatedRoute, Router} from '@angular/router';
import {MatDialog} from '@angular/material/dialog';
import {ManagePanelComponent} from '../manage-panel/manage-panel.component';
import {AuthPanelComponent} from '../auth-panel/auth-panel.component';

@Component({
  selector: 'app-thread',
  templateUrl: './thread.component.html',
  styleUrls: ['./thread.component.scss']
})
export class ThreadComponent implements OnInit {
  public thread: any;
  public posts: any;
  public authUserID;
  public isThreadMember;

  constructor(
	  private configService: ConfigService,
	  private route: ActivatedRoute,
	  private router: Router,
	  private dialog: MatDialog
  ) {
  }

  ngOnInit(): void {
	this.route.params.subscribe(res => {
	  if (res.thread_id) {
		this.getThread(res.thread_id);
	  }
	});

	this.configService.user.asObservable().subscribe(res => {
	  this.authUserID = res?.id;
	  if (this.thread) {
		this.getThreadPosts();
		this.threadMemberExists();
	  }
	});
  }

  public edit(): void {
	const dialogRef = this.dialog.open(ManagePanelComponent, {
	  data: {type: 'thread', item: this.thread}
	});

	dialogRef.afterClosed().subscribe(res => {
	  if (res && res.item && res.updated) {
		this.thread = res.item;
	  } else if (res?.deleted) {
		this.router.navigate(['/home']);
	  }
	});
  }

  public createPost(): void {
	const dialogRef = this.dialog.open(ManagePanelComponent, {
	  data: {type: 'post', item: {thread_id: this.thread.id}}
	});

	dialogRef.afterClosed().subscribe(res => {
	  if (res && res.item && res.new) {
		this.posts.unshift(res.item);
		this.thread.post_count = this.thread.post_count + 1;
	  }
	});
  }

  public toggleJoin(): void {
	if (this.authUserID) {
	  const method = this.isThreadMember ? 'delete' : 'post';
	  this.configService.xanoAPI('/thread_members', method, null,
		  {params: {thread_id: this.thread.id}}, true)
		  .subscribe(
			  res => this.isThreadMember = !this.isThreadMember,
			  error => this.configService.showErrorSnack(error)
		  );
	} else {
	  this.dialog.open(AuthPanelComponent);
	}
  }

  private getThread(threadID): any {
	this.configService.xanoAPI('/thread/' + threadID, 'get').subscribe(
		res => {
		  this.thread = res;
		  this.threadMemberExists();
		  this.getThreadPosts();
		},
		error => this.configService.showErrorSnack(error)
	);
  }

  private getThreadPosts(): any {
	const expression = this.configService.searchColumn([
	  {operator: '=', query: this.thread.id, column: 'post.thread_id'},
	  {operator: '=', query: null, column: 'post.parent_post_id'},
	]);

	this.configService.xanoAPI('/post', 'get', null, {
	  params: {search: JSON.stringify({expression}), user_id: this.authUserID ? this.authUserID : -1}
	}).subscribe(
		res => this.posts = res?.items,
		error => this.configService.showErrorSnack(error)
	);
  }

  private threadMemberExists(): void {
	if (this.authUserID && this.thread) {
	  this.configService.xanoAPI('/thread_members', 'get', null,
		  {params: {thread_id: this.thread.id}}, true)
		  .subscribe(res => {
			this.isThreadMember = res;
		  }, error => this.configService.showErrorSnack(error));
	}
  }

}
