import {Component, Input, OnInit, Output} from '@angular/core';
import {ControlSchema} from '../_demo-core/form-generator/form-generator.component';
import {FormGroup, Validators} from '@angular/forms';
import {ConfigService} from '../_demo-core/config.service';
import {MatIcon} from '@angular/material/icon';

@Component({
  selector: 'app-comment',
  templateUrl: './comment.component.html',
  styleUrls: ['./comment.component.scss']
})
export class CommentComponent implements OnInit {

  public form: FormGroup;
  public postFormSchema: ControlSchema[] = [
	{name: 'thread_id', type: 'hidden', validators: ['required']},
	{name: 'post_id', type: 'hidden'},
	{name: 'parent_post_id', type: 'hidden'},
	{
	  name: 'content', type: 'textarea', label: 'Comment', textarea_rows: 5, validators: [
		Validators.minLength(2)]
	}
  ];
  public votes: number = 0;
  public editing: boolean;
  public isCommentOwner: boolean;
  public userVote: string;
  @Input() comment: any;
  @Output() deleted: boolean;

  constructor(
	  private configService: ConfigService
  ) {
  }

  ngOnInit(): void {
	this.votes = this.comment?.vote_tally?.up_or_down ? this.comment.vote_tally.up_or_down : 0;
	this.configService.user.asObservable().subscribe(res => {
	  if (res) {
		this.isCommentOwner = res.id && this.comment.creator_id === res.id;
	  }
	});
  }

  public delete(): void {
	this.configService.xanoAPI('/post/' + this.comment.id, 'delete', null, {}, true)
		.subscribe(
			res => this.comment = null,
			error => this.configService.showErrorSnack(error)
		);
  }

  public toggleEdit(): void {
	this.editing = !this.editing;
  }

  public submit(): void {
	this.form.get('post_id').patchValue(this.comment.id);
	this.form.markAllAsTouched();

	if (this.form.valid) {
	  this.configService.xanoAPI('/post', 'post', this.form.getRawValue(), {}, true)
		  .subscribe(
			  res => {
				this.comment = res;
				this.editing = false;
			  },
			  error => this.configService.showErrorSnack(error)
		  );
	}
  }

  public vote(direction, clickedIcon: MatIcon, oppositeIcon: MatIcon): void {
	if (this.configService.user.value) {
	  this.configService.xanoAPI('/vote', 'post', {post_id: this.comment.id, up_or_down: direction},
		  {}, true)
		  .subscribe(
			  res => {
				this.votes = res?.vote_tally?.up_or_down ? res.vote_tally.up_or_down : 0;
				if ((direction === 'up' && this.comment?.user_vote?.up_or_down === 1) ||
					(direction === 'down' && this.comment?.user_vote?.up_or_down === -1) || this.votes === 0) {
				  clickedIcon.color = null;
				} else {
				  clickedIcon.color = direction === 'up' ? 'primary' : 'warn';
				}
				oppositeIcon.color = null;
			  },
			  error => this.configService.showErrorSnack(error)
		  );
	} else {
	  this.configService.showErrorSnack({error: {message: 'You must be logged in to vote!'}});
	}
  }

  public userVoteRecord(direction): string {
	if (direction === 'up' && this.comment?.user_vote?.up_or_down === 1) {
	  return 'primary';
	} else if (direction === 'down' && this.comment?.user_vote?.up_or_down === -1) {
	  return 'warn';
	}

	return '';
  }

  public directionOfVotes(): string {
	if (this.votes > 0) {
	  return 'primary';
	} else if (this.votes < 0) {
	  return 'warn';
	} else {
	  return '';
	}
  }
}
