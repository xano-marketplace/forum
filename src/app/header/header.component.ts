import {Component, OnInit} from '@angular/core';
import {ConfigService} from '../_demo-core/config.service';
import {FormControl} from '@angular/forms';
import {DemoConfig} from '../_demo-core/_demo-core';
import {AuthPanelComponent} from '../auth-panel/auth-panel.component';
import {MatDialog} from '@angular/material/dialog';
import {ManagePanelComponent} from '../manage-panel/manage-panel.component';
import {Router} from '@angular/router';
import {debounceTime, switchMap, tap} from 'rxjs/operators';
import {EMPTY} from 'rxjs';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})

export class HeaderComponent implements OnInit {
  public apiConfigured: boolean;
  public loggedIn: boolean;
  public config: DemoConfig;
  public username: string;
  public userID: number;
  public search: FormControl = new FormControl('');
  public filteredResults: any;

  constructor(
	  private configService: ConfigService,
	  private dialog: MatDialog,
	  private router: Router
  ) {
  }

  ngOnInit(): void {
	this.config = this.configService.config;
	this.configService.user.asObservable().subscribe(res => {
	  this.username = res?.username;
	  this.userID = res?.id;
	});
	this.configService.isConfigured().subscribe(apiUrl => this.apiConfigured = !!apiUrl);
	this.configService.isLoggedIn().subscribe(res => this.loggedIn = res);

	this.search.valueChanges.pipe(
		debounceTime(300),
		tap(() => {
		  if (!this.search.value) {
			this.filteredResults = [];
		  }
		}),
		switchMap(value => {
		  if (value) {
			const expression = this.configService.searchColumn([
			  {operator: 'includes', query: value, column: 'thread.subject'}
			]);

			return this.configService.xanoAPI('/thread', 'get', null, {
			  params: {external: JSON.stringify({expression})}
			});
		  } else {
			return EMPTY;
		  }
		})
	)
		.subscribe(result => {
		  this.filteredResults = result?.items;
		});
  }

  public logout(): void {
	this.configService.authToken.next(null);
	this.configService.user.next(null);
  }

  public showPanel(dialogType, signup = false): void {
	let dialogRef;
	switch (dialogType) {
	  case 'auth':
		dialogRef = this.dialog.open(AuthPanelComponent, {data: {signup}});
		break;
	  case 'manage':
		dialogRef = this.dialog.open(ManagePanelComponent, {
		  minWidth: '50vw', data: {
			type: 'thread'
		  }
		});
		dialogRef.afterClosed().subscribe(res => {
		  if (res && res.item && res.new) {
			this.router.navigate(['/thread', res.item.id]);
		  }
		});
		break;
	  default:
		break;
	}
  }

}
