import {AfterViewInit, Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {FormGroup, Validators} from '@angular/forms';
import {ConfigService} from '../_demo-core/config.service';
import {ControlSchema} from '../_demo-core/form-generator/form-generator.component';
import {MatSnackBar} from '@angular/material/snack-bar';

@Component({
  selector: 'app-manage-panel',
  templateUrl: './manage-panel.component.html',
  styleUrls: ['./manage-panel.component.scss']
})
export class ManagePanelComponent implements OnInit, AfterViewInit {

  public form: FormGroup;
  public isImagePost: boolean;
  public bannerImage: any = this.data?.item?.banner_image;
  public profileImage: any = this.data?.item?.profile_image;
  public image: any = this.data?.item?.image;
  public threadFormSchema: ControlSchema[] = [
	{name: 'thread_id', type: 'hidden'},
	{name: 'banner_image', type: 'hidden'},
	{name: 'profile_image', type: 'hidden'},
	{name: 'subject', type: 'text', label: 'Subject', validators: ['required']},
	{name: 'guidelines', type: 'textarea', label: 'Guidelines', textarea_rows: 5},
  ];
  public postFormSchema: ControlSchema[] = [
	{name: 'thread_id', type: 'hidden', validators: ['required']},
	{name: 'post_id', type: 'hidden'},
	{name: 'parent_post_id', type: 'hidden'},
	{name: 'image', type: 'hidden'},
	{name: 'title', type: 'text', label: 'Title', validators: ['required', Validators.maxLength(300)]},
	{name: 'content', type: 'textarea', label: 'Content', textarea_rows: 5, validators: [Validators.minLength(2)]}
  ];

  constructor(
	  private configService: ConfigService,
	  private dialogRef: MatDialogRef<ManagePanelComponent>,
	  private snackBar: MatSnackBar,
	  @Inject(MAT_DIALOG_DATA) public data) {
  }

  ngOnInit(): void {
  }

  ngAfterViewInit(): void {
	if (this.data?.item?.image) {
	  this.toggleImagePost('image');
	}
	if (this.data?.type === 'thread') {
	  this.form.get('thread_id').patchValue(this.data?.item?.id ? this.data.item.id : null);
	} else if (this.data?.type === 'post') {
	  this.form.get('post_id').patchValue(this.data?.item?.id ? this.data.item.id : null);
	}
  }

  public upload(formData, controlName): void {
	this.configService.xanoAPI('/upload/image', 'post', formData).subscribe(
		res => this.form.get(controlName).patchValue(res),
		error => this.configService.showErrorSnack(error)
	);
  }

  public deleteImage(controlName): void {
	this.form.get(controlName).patchValue(null);
  }

  public toggleImagePost(type): void {
	this.isImagePost = type === 'image';
	if (!this.isImagePost) {
	  this.form.get('image').patchValue(null);
	  this.postFormSchema.map(x => {
		if (x.name === 'content') {
		  x.type = 'textarea';
		}
		return x;
	  });
	} else {
	  this.form.get('content').patchValue(null);
	  this.postFormSchema.map(x => {
		if (x.name === 'content') {
		  x.type = 'hidden';
		}
		return x;
	  });

	}
  }

  public submit(): void {
	this.form.markAllAsTouched();
	if (this.form.valid && this.data?.type) {
	  const endpoint = this.data.type === 'thread' ? '/thread' : '/post';
	  this.configService.xanoAPI(endpoint, 'post', this.form.getRawValue(), {}, true)
		  .subscribe(
			  res => {
				const message = (this.data?.type === 'thread' ? 'Thread ' : 'Post ') + (this.data?.item?.id ? 'Update' : 'Saved');
				this.snackBar.open(message, 'Success');
				this.dialogRef.close({updated: this.data?.item?.id, new: !this.data?.item?.id, item: res});
			  },
			  error => this.configService.showErrorSnack(error)
		  );
	}
  }

  public delete(): void {
	if (this.data?.type && this.data?.item?.id) {
	  const endpoint = (this.data.type === 'thread' ? '/thread/' : '/post/') + this.data.item.id;
	  this.configService.xanoAPI(endpoint, 'delete', null, {}, true)
		  .subscribe(res => {
			const message = (this.data?.type === 'thread' ? 'Thread ' : 'Post ') + 'Delete';
			this.snackBar.open(message, 'Success');
			this.dialogRef.close({deleted: true, item: res});
		  }, error => this.configService.showErrorSnack(error));
	}
  }

}
