import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {HeaderComponent} from './header/header.component';
import {HomeComponent} from './home/home.component';
import {HttpClientModule} from '@angular/common/http';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatButtonModule} from '@angular/material/button';
import {AuthPanelComponent} from './auth-panel/auth-panel.component';
import {ManagePanelComponent} from './manage-panel/manage-panel.component';
import {MatCardModule} from '@angular/material/card';
import {MatInputModule} from '@angular/material/input';
import {MAT_DIALOG_DEFAULT_OPTIONS, MatDialogModule} from '@angular/material/dialog';
import {ReactiveFormsModule} from '@angular/forms';
import {MatIconModule} from '@angular/material/icon';
import {MAT_SNACK_BAR_DEFAULT_OPTIONS, MatSnackBarModule} from '@angular/material/snack-bar';
import {MatCommonModule} from '@angular/material/core';
import {MatDividerModule} from '@angular/material/divider';
import {MatExpansionModule} from '@angular/material/expansion';
import {DemoCoreModule} from './_demo-core/demo-core.module';
import {PostComponent} from './post/post.component';
import {ThreadComponent} from './thread/thread.component';
import {MatMenuModule} from '@angular/material/menu';
import {AppRouterOutletComponent} from './app-router-outlet/app-router-outlet.component';
import {MatBadgeModule} from '@angular/material/badge';
import {MatTabsModule} from '@angular/material/tabs';
import {MomentModule} from 'ngx-moment';
import {PostStreamComponent} from './post-stream/post-stream.component';
import {UserProfileComponent} from './user-profile/user-profile.component';
import {MatListModule} from '@angular/material/list';
import {EmptyProfilePictureComponent} from './empty-profile-picture/empty-profile-picture.component';
import {MatButtonToggleModule} from '@angular/material/button-toggle';
import {CommentComponent} from './comment/comment.component';
import {MatAutocompleteModule} from '@angular/material/autocomplete';

@NgModule({
  declarations: [
	AppComponent,
	AppRouterOutletComponent,
	HomeComponent,
	HeaderComponent,
	AuthPanelComponent,
	ManagePanelComponent,
	PostComponent,
	ThreadComponent,
	PostStreamComponent,
	UserProfileComponent,
	EmptyProfilePictureComponent,
	CommentComponent,
  ],
  imports: [
	BrowserModule,
	AppRoutingModule,
	MatCommonModule,
	HttpClientModule,
	DemoCoreModule,
	BrowserAnimationsModule,
	ReactiveFormsModule,
	MatToolbarModule,
	MatMenuModule,
	MatIconModule,
	MatButtonModule,
	MomentModule,
	MatListModule,
	MatButtonToggleModule,
	MatAutocompleteModule,
	MatSnackBarModule,
	MatCardModule,
	MatInputModule,
	MatDialogModule,
	MatToolbarModule,
	MatDividerModule,
	MatBadgeModule,
	MatTabsModule,
	MatExpansionModule
  ],
  providers: [{
	provide: MAT_SNACK_BAR_DEFAULT_OPTIONS,
	useValue: {
	  duration: 4000,
	  horizontalPosition: 'start'
	}
  }, {
	provide: MAT_DIALOG_DEFAULT_OPTIONS,
	useValue: {
	  hasBackdrop: true,
	  minHeight: '100vh',
	  position: {top: '0px', right: '0px'},
	  panelClass: 'slide-out-panel'
	}
  }],
  bootstrap: [AppComponent]
})
export class AppModule {
}
