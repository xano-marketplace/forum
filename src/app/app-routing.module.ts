import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {HomeComponent} from './home/home.component';
import {DemoLandingComponent} from './_demo-core/demo-landing/demo-landing.component';
import {ConfigGuard} from './_demo-core/config.guard';
import {ThreadComponent} from './thread/thread.component';
import {PostStreamComponent} from './post-stream/post-stream.component';
import {UserProfileComponent} from './user-profile/user-profile.component';

const routes: Routes = [
  {path: '', component: DemoLandingComponent},
  {path: 'home', component: HomeComponent, canActivate: [ConfigGuard]},
  {path: 'thread', component: ThreadComponent, canActivate: [ConfigGuard]},
  {path: 'thread/:thread_id', component: ThreadComponent, canActivate: [ConfigGuard]},
  {path: 'post', component: PostStreamComponent, canActivate: [ConfigGuard]},
  {path: 'post/:post_id', component: PostStreamComponent, canActivate: [ConfigGuard]},
  {path: 'user', component: UserProfileComponent, canActivate: [ConfigGuard]},
  {path: 'user/:user_id', component: UserProfileComponent, canActivate: [ConfigGuard]}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
