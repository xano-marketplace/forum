import {Component, OnInit} from '@angular/core';
import {ConfigService} from '../_demo-core/config.service';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.scss']
})
export class UserProfileComponent implements OnInit {

  public user: any;
  public posts: any;
  private userId: number;

  constructor(
	  private configService: ConfigService,
	  private route: ActivatedRoute,
	  private router: Router) {
  }

  ngOnInit(): void {
	this.route.params.subscribe(res => {
	  this.userId = res?.user_id;
	  if (this.userId) {
		this.getUser();
		this.getUserPosts();
	  }
	});

  }

  public getUser(): void {
	this.configService.xanoAPI('/user/' + this.userId, 'get').subscribe(
		res => {
		  this.user = res;
		},
		error => {
		  this.router.navigate(['/home']);
		  this.configService.showErrorSnack(error);
		}
	);
  }

  public getUserPosts(): void {
	const expression = this.configService.searchColumn([
	  {operator: '=', query: this.userId, column: 'post.creator_id'},
	  {operator: '=', query: null, column: 'post.parent_post_id'},
	]);
	this.configService.xanoAPI('/post', 'get', null, {
	  params: {search: JSON.stringify({expression})}
	}).subscribe(
		res => this.posts = res?.items,
		error => this.configService.showErrorSnack(error)
	);
  }
}
