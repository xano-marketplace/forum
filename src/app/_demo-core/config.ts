import {DemoConfig} from './_demo-core';

export const config: DemoConfig = {
  title: 'Forum',
  summary: '',
  marketplace_type: 'template',
  editLink: 'https://gitlab.com/xano-marketplace/forum/',
  components: [
	{
	  name: 'Home',
	  description: 'This component will show the newest threads widget and has a wall that will either show new posts or (if logged in) posts of threads you have joined.'
	},
	{
	  name: 'Thread',
	  description: 'This component allows users to view a thread\'s posts and guidelines or join a thread.'
	},
	{
	  name: 'Post',
	  description: 'This component will show the full content of a post and comments associated with the post. Users can also create new comments on the post.'
	},
	{
	  name: 'Profile',
	  description: 'This component shows all posts a user has made.'
	},
	{
	  name: 'Auth Panel',
	  description: 'This panel handles signup and login.'
	},
	{
	  name: 'Manage Panel',
	  description: 'This panel handles both thread and post-management, where a user can create, edit, or delete a thread or post.'
	},
  ],
  instructions: [
	'Install the template in your Xano Workspace',
	'Go to the newly added Forum API Group and copy your API BASE URL',
	'In this demo, paste this API BASE URL in as "Your Xano API URL'
  ],
  requiredApiPaths: [
	'/auth/login',
	'/auth/signup',
	'/auth/me',
	'/post',
	'/post/{post_id}',
	'/thread',
	'/thread/{thread_id}',
	'/thread_members',
	'/upload/image',
	'/user',
	'/user/{user_id}'
  ]
};
