import {AfterViewInit, Component, OnInit} from '@angular/core';
import {ConfigService} from '../_demo-core/config.service';
import {ActivatedRoute} from '@angular/router';
import {ControlSchema} from '../_demo-core/form-generator/form-generator.component';
import {FormGroup, Validators} from '@angular/forms';
import {MatIcon} from '@angular/material/icon';

@Component({
  selector: 'app-post-stream',
  templateUrl: './post-stream.component.html',
  styleUrls: ['./post-stream.component.scss']
})
export class PostStreamComponent implements OnInit, AfterViewInit {

  public post: any;
  public postOfPost: any;
  public username = null;
  public votes: number = 0;
  public form: FormGroup;
  public postFormSchema: ControlSchema[] = [
	{name: 'thread_id', type: 'hidden', validators: ['required']},
	{name: 'post_id', type: 'hidden'},
	{name: 'parent_post_id', type: 'hidden'},
	{
	  name: 'content', type: 'textarea', disabled: true, label: 'Login to comment', textarea_rows: 5, validators: [
		Validators.minLength(2)]
	}
  ];

  constructor(
	  private configService: ConfigService,
	  private route: ActivatedRoute) {
  }

  ngOnInit(): void {
	this.configService.user.asObservable().subscribe(res => {
	  this.username = res?.username;
	  if (this.username) {
		this.postFormSchema = this.postFormSchema.map(x => {
		  if (x.name === 'content') {
			x.disabled = false;
			x.label = 'Comment as ' + this.username;
		  }
		  return x;
		});
	  }
	});
	this.route.params.subscribe(res => {
	  if (res.post_id) {
		this.getPost(res.post_id);
	  }
	});
  }

  ngAfterViewInit(): void {
  }

  public comment(): void {
	if (this.post) {
	  this.form.patchValue({
		thread_id: this.post.thread_id,
		parent_post_id: this.post.id
	  });
	}
	if (this.form.valid) {
	  this.configService.xanoAPI('/post', 'post', this.form.getRawValue(), {}, true)
		  .subscribe(res => {
				const post = {...res, creator: {username: this.username}};
				this.postOfPost.unshift(post);
				this.form.reset();
			  }, error => this.configService.showErrorSnack(error)
		  );
	}
  }

  public vote(direction, clickedIcon: MatIcon, oppositeIcon: MatIcon): void {
	if (this.configService.user.value) {
	  this.configService.xanoAPI('/vote', 'post', {post_id: this.post.id, up_or_down: direction},
		  {}, true)
		  .subscribe(
			  res => {
				this.votes = res?.vote_tally?.up_or_down ? res.vote_tally.up_or_down : 0;
				if ((direction === 'up' && this.post?.user_vote?.up_or_down === 1) ||
					(direction === 'down' && this.post?.user_vote?.up_or_down === -1) || this.votes === 0) {
				  clickedIcon.color = null;
				} else {
				  clickedIcon.color = direction === 'up' ? 'primary' : 'warn';
				}
				oppositeIcon.color = null;
			  },
			  error => this.configService.showErrorSnack(error)
		  );
	} else {
	  this.configService.showErrorSnack({error: {message: 'You must be logged in to vote!'}});
	}
  }

  public userVoteRecord(direction): string {
	if (direction === 'up' && this.post?.user_vote?.up_or_down === 1) {
	  return 'primary';
	} else if (direction === 'down' && this.post?.user_vote?.up_or_down === -1) {
	  return 'warn';
	}

	return '';
  }

  public directionOfVotes(): string {
	if (this.votes > 0) {
	  return 'primary';
	} else if (this.votes < 0) {
	  return 'warn';
	} else {
	  return '';
	}
  }

  private getPost(postID): any {
	this.configService.xanoAPI('/post/' + postID, 'get').subscribe(res => {
	  this.post = res;
	  this.getPostOfPost();
	}, error => this.configService.showErrorSnack(error));
  }

  private getPostOfPost(): any {
	const expression = this.configService.searchColumn([
	  {operator: '=', query: this.post?.id, column: 'post.parent_post_id'},
	]);

	this.configService.xanoAPI('/post', 'get', null, {
	  params: {search: JSON.stringify({expression})}
	}).subscribe(
		res => this.postOfPost = res?.items,
		error => this.configService.showErrorSnack(error)
	);
  }
}
