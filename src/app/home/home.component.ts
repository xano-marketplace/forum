import {Component, OnInit} from '@angular/core';
import {ConfigService} from '../_demo-core/config.service';
import {DemoConfig} from '../_demo-core/_demo-core';
import {FormGroup} from '@angular/forms';
import {mergeMap} from 'rxjs/operators';
import {of} from 'rxjs';
import {BreakpointObserver, Breakpoints} from '@angular/cdk/layout';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  constructor(
	  private configService: ConfigService,
	  private breakpointObserver: BreakpointObserver
  ) {
	breakpointObserver.observe([Breakpoints.Small, Breakpoints.XSmall, Breakpoints.Medium]).subscribe(res => {
	  this.isSmallScreen = res.matches;
	});
  }

  public config: DemoConfig;
  public configured: boolean = false;
  public threads: any;
  public posts: any;
  public form: FormGroup;
  public isSmallScreen: boolean;

  ngOnInit(): void {
	this.config = this.configService.config;
	this.configService.xanoApiUrl.subscribe(apiUrl => this.configured = !!apiUrl);
	this.getNewestThreads();
	this.getWall();

	this.configService.user.asObservable().subscribe(res => {
	  this.getWall();
	});
  }

  public getNewestThreads(): void {
	this.configService.xanoAPI('/thread', 'get').subscribe(
		res => {
		  this.threads = res.items;
		},
		error => this.configService.showErrorSnack(error)
	);
  }

  public getWall(): void {
	const expression = this.configService.searchColumn([
	  {operator: '=', query: null, column: 'post.parent_post_id'},
	]);

	if (this.configService.user.value) {
	  this.configService.xanoAPI('/wall', 'get', null, {params: {search: JSON.stringify({expression})}}, true)
		  .pipe(mergeMap((posts) => {
			return posts?.items.length > 0 ? of(posts) : this.configService.xanoAPI('/post', 'get', null, {});
		  }))
		  .subscribe(
			  res => this.posts = res.items,
			  error => this.configService.showErrorSnack(error)
		  );
	} else {
	  this.configService.xanoAPI('/post', 'get', null, {params: {search: JSON.stringify({expression})}}).subscribe(
		  res => this.posts = res.items,
		  error => this.configService.showErrorSnack(error)
	  );
	}
  }

}

