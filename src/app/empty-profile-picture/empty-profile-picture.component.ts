import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-empty-profile-picture',
  templateUrl: './empty-profile-picture.component.html',
  styleUrls: ['./empty-profile-picture.component.scss']
})
export class EmptyProfilePictureComponent implements OnInit {

  @Input() height: string;
  @Input() width: string;
  @Input() type: string;

  constructor() {
  }

  ngOnInit(): void {
  }

}
