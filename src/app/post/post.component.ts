import {Component, Input, OnInit} from '@angular/core';
import {ConfigService} from '../_demo-core/config.service';
import {MatDialog} from '@angular/material/dialog';
import {ManagePanelComponent} from '../manage-panel/manage-panel.component';
import {MatIcon} from '@angular/material/icon';

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.scss']
})
export class PostComponent implements OnInit {
  constructor(
	  private configService: ConfigService,
	  private dialog: MatDialog
  ) {
  }

  @Input() post: any;
  @Input() showThread: boolean = true;
  @Input() truncate: boolean;
  @Input() showCommentButton: boolean = true;
  public votes: number = 0;
  public isPostOwner: boolean;

  ngOnInit(): void {
	this.votes = this.post?.vote_tally?.up_or_down ? this.post.vote_tally.up_or_down : 0;

	this.configService.user.asObservable().subscribe(res => {
	  if (res) {
		this.isPostOwner = res.id && this.post.creator_id === res.id;
	  }
	});
  }

  public vote(direction, clickedIcon: MatIcon, oppositeIcon: MatIcon): void {
	if (this.configService.user.value) {
	  this.configService.xanoAPI('/vote', 'post', {post_id: this.post.id, up_or_down: direction},
		  {}, true)
		  .subscribe(
			  res => {
				this.votes = res?.vote_tally?.up_or_down ? res.vote_tally.up_or_down : 0;
				if ((direction === 'up' && this.post?.user_vote?.up_or_down === 1) ||
					(direction === 'down' && this.post?.user_vote?.up_or_down === -1) || this.votes === 0) {
				  clickedIcon.color = null;
				} else {
				  clickedIcon.color = direction === 'up' ? 'primary' : 'warn';
				}
				oppositeIcon.color = null;
			  },
			  error => this.configService.showErrorSnack(error)
		  );
	} else {
	  this.configService.showErrorSnack({error: {message: 'You must be logged in to vote!'}});
	}
  }

  public edit(): void {
	const dialogRef = this.dialog.open(ManagePanelComponent, {data: {type: 'post', item: this.post}});

	dialogRef.afterClosed().subscribe(res => {
	  if (res && res.updated && res.item) {
		this.post = res.item;
	  } else if (res?.deleted) {
		this.post = null;
	  }
	});
  }

  public userVoteRecord(direction): string {
	if (direction === 'up' && this.post?.user_vote?.up_or_down === 1) {
	  return 'primary';
	} else if (direction === 'down' && this.post?.user_vote?.up_or_down === -1) {
	  return 'warn';
	}
	return '';
  }

  public directionOfVotes(): string {
	if (this.votes > 0) {
	  return 'primary';
	} else if (this.votes < 0) {
	  return 'warn';
	} else {
	  return '';
	}
  }
}
